# Step to run Strapi Project with docker

# Pre-requirement 
    1. Docker
    2. Docker Compose

# 1 Setup Environment
    1. Copy and rename file .env.example to .env
    2. Config all the variable
        NODE_ENV => state of project (development, production)
        HOST => set host for strapi
        PORT => set port for strapi
        DATABASE => Database 
        DATABASE_HOST => Database host (if in the docker use database's service name)
        DATABASE_PORT => Port of database
        DATABASE_NAME => Database name
        DATABASE_USERNAME => Database username
        DATABASE_PASSWORD => Database password
# 2 Create docker network
    run command 
    docker network create database 
    docker network create web
# 3 Start strapi 
    run command 
    docker-compose up --build -d

# Step to Strapi Project without docker

# Pre-requirement 
    1. Database server (Postgres, MySql)
    2. Node
    3. NPM or Yarn

# 1 Create Database for Strapi
    1. Use command / ui to create Database for strapi
    2. Create username and password for strapi
# 2 Setup Environment
    1. Copy and rename file .env.example to .env
    2. Config all the variable
        NODE_ENV => state of project (development, production)
        HOST => set host for strapi
        PORT => set port for strapi
        DATABASE => Database 
        DATABASE_HOST => Database host (if in the docker use database's service name)
        DATABASE_PORT => Port of database
        DATABASE_NAME => Database name
        DATABASE_USERNAME => Database username
        DATABASE_PASSWORD => Database password

# 3. Install NPM/Yarn
    run command
    NPM: npm install
    Yarn: yarn 
# 4. Start Strapi on develop mode
    run command 
    NPM: npm run develop
    Yarn: yarn develop

# 4. Start Strapi on production mode
    run command 
    NPM: npm start
    Yarn: yarn start